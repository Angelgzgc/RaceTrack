#
# This is a project Makefile. It is assumed the directory this Makefile resides in is a
# project subdirectory.
#

PROJECT_NAME := app-template
COMPONENT_SRCDIRS := Sensors Comms Peripherals ThirdParty ../Common Utils
COMPONENT_EXTRA_INCLUDES := ../Common main/Utils
EXTRA_COMPONENT_DIRS := Common

include $(IDF_PATH)/make/project.mk

