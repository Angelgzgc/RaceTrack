/*
 * Utils.cpp
 *
 *  Created on: 6 jul. 2018
 *      Author: gonzalezag
 */

#include "Utils.h"
#include "esp_log.h"
#include <cmath>

Utils::Utils() {
    // TODO Auto-generated constructor stub

}

Utils::~Utils() {
    // TODO Auto-generated destructor stub
}

void Utils::toEulerAngle(double qw, double qx, double qy, double qz, double& roll, double& pitch, double& yaw){

	//ESP_LOGI("[DEBUG EULER]", "Qs: %.2f %.2f %.2f %.2f\n", qw, qx, qy, qz);

    // roll (x-axis rotation)
    double sinr = +2.0 * (qw * qx + qy * qz);
    double cosr = +1.0 - 2.0 * (qx * qx + qy * qy);
    roll = atan2(sinr, cosr);

    // pitch (y-axis rotation)
    double sinp = +2.0 * (qw * qy - qz * qx);
    if (fabs(sinp) >= 1)
        pitch = copysign(M_PI / 2, sinp); // use 90 degrees if out of range
    else
        pitch = asin(sinp);

    // yaw (z-axis rotation)
    double siny = +2.0 * (qw * qz + qx * qy);
    double cosy = +1.0 - 2.0 * (qy * qy + qz * qz);
    yaw = atan2(siny, cosy);
}
