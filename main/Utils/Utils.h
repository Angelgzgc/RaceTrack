/*
 * Utils.h
 *
 *  Created on: 6 jul. 2018
 *      Author: gonzalezag
 */

#ifndef MAIN_UTILS_UTILS_H_
#define MAIN_UTILS_UTILS_H_

#ifdef PROFILE
	#define PROFILE_INIT uint64_t init_time;
	#define PROFILE(name, x) init_time = esp_timer_get_time(); \
		x; \
		ESP_LOGI("[" name  "]", "%" PRId64 , (esp_timer_get_time() - init_time));

	#define PROFILE_START init_time = esp_timer_get_time();
	#define PROFILE_END(name) ESP_LOGI("[" name  "]", "%" PRId64 , (esp_timer_get_time() - init_time));
#else
	#define PROFILE_INIT
	#define PROFILE(name, x) x;
	#define PROFILE_START
	#define PROFILE_END(name)
#endif

class Utils {
public:
    Utils();
    ~Utils();

    static void toEulerAngle(double qw, double qx, double qy, double qz, double& roll, double& pitch, double& yaw);

};

#endif /* MAIN_UTILS_UTILS_H_ */
