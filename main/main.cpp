#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "esp_wifi.h"
#include "esp_log.h"
#include "esp_event_loop.h"
#include "nvs_flash.h"
#include "driver/uart.h"
#include "driver/spi_master.h"
#include "esp_vfs_fat.h"
#include "sdmmc_cmd.h"

#include "Sensors/imu.h"
#include "Sensors/gps.h"

#include "Comms/UARTComm.h"

#include "Peripherals/SDCard.h"

#include <string.h>

//Comms
UARTComm uart1;

//Sensors
Imu imu(0);
Gps gps(0);

//Peripherals
SDCard sd;


extern "C" void app_main()
{

	/*
	 * Init of both I2C interfaces
	 */
	ESP_ERROR_CHECK( i2c0.begin(GPIO_NUM_21, GPIO_NUM_22, (gpio_pullup_t)0x1, (gpio_pullup_t)0x1, 400000));
	i2c0.setTimeout(10);
	i2c0.scanner();

	/*ESP_ERROR_CHECK( i2c1.begin(GPIO_NUM_19, GPIO_NUM_18, (gpio_pullup_t)0x1, (gpio_pullup_t)0x1, 400000));
	i2c1.setTimeout(10);
	i2c1.scanner();*/

	/*
	 * Init UART interface
	 */
	uart_comm_config uart_conf;
	uart_conf.tx_pin 		= GPIO_NUM_17;
	uart_conf.rx_pin 		= GPIO_NUM_16;
	uart_conf.buffer_size	= 1000;
	uart_conf.baud_rate		= 9600;
	uart_conf.uart_number	= UART_NUM_2;

	uart1.Init(&uart_conf);

    /*gpio_config_t io_conf;
    //disable interrupt
    io_conf.intr_type = GPIO_PIN_INTR_DISABLE;
    //set as output mode
    io_conf.mode = GPIO_MODE_OUTPUT;
    //bit mask of the pins that you want to set,e.g.GPIO18/19
    io_conf.pin_bit_mask = GPIO_OUTPUT_PIN_SEL;
    //disable pull-down mode
    io_conf.pull_down_en = 0;
    //disable pull-up mode
    io_conf.pull_up_en = 0;
    //configure GPIO with the given settings
    gpio_config(&io_conf);*/

	/*
	 * Init sensors and peripherals
	 */

	sd.Init();
	sd.Open("/sdcard/test.txt");
	imu.Init(&i2c0);
	gps.Init(&uart1);


	gpio_pad_select_gpio(GPIO_NUM_2);
	gpio_pad_select_gpio(GPIO_NUM_13);
	gpio_set_direction(GPIO_NUM_2, GPIO_MODE_OUTPUT);
	gpio_set_direction(GPIO_NUM_13, GPIO_MODE_OUTPUT);


	imu_output imudata;
	char gpsdata[300];

	int64_t count_init;
	int64_t count_end;
	int64_t flushCounter = esp_timer_get_time();
	while(1){

		count_init =  esp_timer_get_time();
		int gpslen = gps.Read(gpsdata);
		count_end =  esp_timer_get_time();
		//ESP_LOGI("[Time]", "GPS: %lld", count_end-count_init);
		//Comprobamos si el GPS tiene datos
		/*if(strstr(gpsdata, ",A,") == 0){
			//ESP_LOGI("[GPS]","Encender!");
			gpio_set_level(GPIO_NUM_13, 1);
		}else{
			gpio_set_level(GPIO_NUM_13, 0);
		}*/



		sd.Write("%lld,1,%s\n", count_init, gpsdata);

		//sd.WriteBytes(gpsdata, gpslen);
		//ESP_LOGI("[GPS]", "Data: %s", gpsdata);
		//ESP_LOGI("[Time]", "GPS->SD: %lld", esp_timer_get_time()-count_end);
		count_end = esp_timer_get_time();
		// Read IMU Data
		imu.Read(&imudata);
		//ESP_LOGI("[Time]", "IMU: %lld", esp_timer_get_time()-count_end);
		count_end = esp_timer_get_time();
		sd.Write("%lld,0,%.5f,%.5f,%.5f,%.5f,%.5f,%.5f\n",
					count_init,
					imudata.gyro[0],
					imudata.gyro[1],
					imudata.gyro[2],
					imudata.accel[0],
					imudata.accel[1],
					imudata.accel[2]);
		//ESP_LOGI("[Time]", "IMU->SD: %lld", esp_timer_get_time()-count_end);
		if( imudata.accel[0] > 2 || imudata.accel[1] > 2 || imudata.accel[2] > 2){
			gpio_set_level(GPIO_NUM_2, 1);
			//ESP_LOGI("[LED]","Encender!");
		}else{
			gpio_set_level(GPIO_NUM_2, 0);
		}

		ESP_LOGI("[IMU]", "Data!\n");

		count_end =  esp_timer_get_time();
		//ESP_LOGI("[Time]", "%lld %lld %lld\n", count_init, count_end, count_end - count_init);
		if( esp_timer_get_time() - flushCounter > 2000000LL){
			flushCounter = esp_timer_get_time();
			sd.flush();
			ESP_LOGI("[SD]"," Flushing!\n");
		}
		if(count_end - count_init < 10000LL)
			usleep(10000 - (count_end - count_init));
		else
			ESP_LOGE("[Time]","Tiempo no cumplido!(%lld)\n",count_end - count_init);

		//ESP_LOGI("Info", "Fin de bucle!\n");
	}


}


