/*
 * Gps.cpp
 *
 *  Created on: 27 jul. 2018
 *      Author: amartinezre
 */

#include "gps.h"

Gps::Gps(uint16_t sensorID) :ISensor(sensorID) {
	// TODO Auto-generated constructor stub

}

Gps::~Gps() {
	// TODO Auto-generated destructor stub
}

/**
 * Initialize the sensor with its configuration
**/
int Gps::Init(void *connection){
	uart = (UARTComm*)connection;
	return 0;
}

/**
 * Read the data from the sensor. The output data
 * shall be stored in readdata.
 **/
int Gps::Read(void *readdata){
	size_t size = 0;
	uart->Read(readdata, &size);
	return size;
}

/**
 * Configure the 'cmd' with the value 'arg'
 **/
int Gps::Config(int cmd, int arg){
	return 0;
}
