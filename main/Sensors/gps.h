/*
 * gps.h
 *
 *  Created on: 27 jul. 2018
 *      Author: amartinezre
 */

#ifndef MAIN_SENSORS_GPS_H_
#define MAIN_SENSORS_GPS_H_

#include "ISensor.h"
#include "../Comms/UARTComm.h"

class Gps: public ISensor {
private:
	UARTComm *uart;
public:
	Gps(uint16_t sensorID);
	virtual ~Gps();

    uint16_t getId(){ return this->SensorID; };

    /**
     * Initialize the sensor with its configuration
    **/
    int Init(void *connection);

    /**
     * Read the data from the sensor. The output data
     * shall be stored in readdata.
     **/
    int Read(void *readdata);

    /**
     * Configure the 'cmd' with the value 'arg'
     **/
    int Config(int cmd, int arg);

    int Close(){return 0;}
};

#endif /* MAIN_SENSORS_GPS_H_ */
