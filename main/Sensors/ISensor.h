#ifndef __ISENSOR_H__
#define __ISENSOR_H__

#include <stdint.h>

class ISensor{
public:
    uint16_t SensorID;

    ISensor(uint16_t sensorID){ this->SensorID = sensorID; };
    ~ISensor(){};

    uint16_t getId(){ return this->SensorID; };

    /**
     * Initialize the sensor with its configuration
    **/
    virtual int Init(void *connection) = 0;

    /**
     * Read the data from the sensor. The output data 
     * shall be stored in readdata.
     **/
    virtual int Read(void *readdata) =0;

    /**
     * Configure the 'cmd' with the value 'arg'
     **/
    virtual int Config(int cmd, int arg) =0;

    
    virtual int Close() =0;
    
};

#endif
