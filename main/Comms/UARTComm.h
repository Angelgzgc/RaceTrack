/*
 * UARTComm.h
 *
 *  Created on: 26 jul. 2018
 *      Author: amartinezre
 */

#ifndef MAIN_COMMS_UARTCOMM_H_
#define MAIN_COMMS_UARTCOMM_H_

#include "IComms.h"
#include <driver/uart.h>
#include "driver/gpio.h"
#include "esp_err.h"

struct uart_comm_config{
	gpio_num_t  tx_pin;
	gpio_num_t  rx_pin;
    uint32_t    buffer_size;
    int32_t     baud_rate;
    uart_port_t uart_number;
};

class UARTComm : public IComms {
private:
	uart_comm_config uart;
	uint8_t data[150];
public:
	UARTComm();
	~UARTComm();

	/**
	 * Initialize the peripheral with its configuration
	**/
	int Init(void *config);

	/**
	 * Read the data from the peripheral. The output data
	 * shall be stored in readdata.
	 **/
	esp_err_t Read(void *readdata, size_t *size);

	/**
	 * Writes data to the peripheral. The input data
	 * shall be stored in readdata.
	 **/
	esp_err_t Write(void *writedata, size_t size);

	/**
	 * Configure the 'cmd' with the value 'arg'
	 **/
	int Config(int cmd, void* conf){ return 0;};


	int Close(){return 0;};
};

#endif /* MAIN_COMMS_UARTCOMM_H_ */
