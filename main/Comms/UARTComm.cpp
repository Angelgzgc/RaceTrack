/*
 * UARTComm.cpp
 *
 *  Created on: 26 jul. 2018
 *      Author: amartinezre
 */

#include "UARTComm.h"
#include "esp_log.h"

#define ECHO_TEST_RTS  (UART_PIN_NO_CHANGE)
#define ECHO_TEST_CTS  (UART_PIN_NO_CHANGE)

UARTComm::UARTComm() {
	// TODO Auto-generated constructor stub

}

UARTComm::~UARTComm() {
	// TODO Auto-generated destructor stub
}

/**
 * Initialize the peripheral with its configuration
**/
int UARTComm::Init(void *config){
    struct uart_comm_config *con;
    con = (struct uart_comm_config*)config;

    uart.tx_pin 	 = con->tx_pin;
    uart.rx_pin      = con->rx_pin;
    uart.buffer_size = con->buffer_size;
    uart.baud_rate   = con->baud_rate;
    uart.uart_number = con->uart_number;

    uart_config_t uart_config = {
        .baud_rate = uart.baud_rate,
        .data_bits = UART_DATA_8_BITS,
        .parity    = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE
    };

    uart_param_config(uart.uart_number, &uart_config);
    uart_set_pin(uart.uart_number, uart.tx_pin, uart.rx_pin, ECHO_TEST_RTS, ECHO_TEST_CTS);
    uart_driver_install(uart.uart_number, uart.buffer_size * 2, 0, 0, NULL, 0);

	return 0;
}

/**
 * Read the data from the peripheral. The output data
 * shall be stored in readdata.
 **/
esp_err_t UARTComm::Read(void *readdata, size_t *size){
	uint8_t* data = (uint8_t*)readdata;
	int len = 0;
	(void)size;
	//ESP_LOGI("[UART]", "Reading data...\n");

	while((len = uart_read_bytes(uart.uart_number, (unsigned char *)data, 1, 0)) >= 1){//portMAX_DELAY);
		if (*data == '\n') {
			*data = '\0';
			size = (size_t*)len;
			//ESP_LOGI("[UART]", "Leido (%d): %s\n", len,data);
			return ESP_OK;
		}
		len++;
		data++;
	}
	return ESP_FAIL;
}

/**
 * Writes data to the peripheral. The input data
 * shall be stored in readdata.
 **/
esp_err_t UARTComm::Write(void *writedata, size_t size){
	uint8_t* data = (uint8_t*)writedata;
	uart_write_bytes(uart.uart_number, (const char *) data, size);
	return ESP_OK;
}

