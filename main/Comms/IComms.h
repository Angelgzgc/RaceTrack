#ifndef __ICOMMS_H__
#define __ICOMMS_H__

#include <stdint.h>
#include <stdio.h>
#include "esp_err.h"

class IComms{
public:

    IComms(){};
    ~IComms(){};

    /**
     * Initialize the peripheral with its configuration
    **/
    virtual int Init(void *config) =0;

    /**
     * Read the data from the peripheral. The output data
     * shall be stored in readdata.
     **/
    virtual esp_err_t Read(void *readdata, size_t *size) =0;

    /**
	 * Writes data to the peripheral. The input data
	 * shall be stored in readdata.
	 **/
	virtual esp_err_t Write(void *writedata, size_t size) =0;

    /**
     * Configure the 'cmd' with the value 'arg'
     **/
    virtual int Config(int cmd, void* conf) =0;


    virtual int Close() =0;

};

#endif
